//
// This is the server configuration tree.
// Guide: https://manual.os-js.org/v3/config/#server
// Complete config tree: https://github.com/os-js/osjs-server/blob/master/sr$
//

const path = require('path');
const root = path.resolve(__dirname, '../../');

module.exports = {
  root,
  port: CUSTOM,
  public: path.resolve(root, 'dist'),
  vfs: { root: '/home' },
 
 xterm: {
    ssh: {
      // Custom hostname
      hostname: 'localhost',

      // Custom port
      args: '-p 22'
    }
  }

 

};
